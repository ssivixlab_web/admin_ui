import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAuthGuardService as AuthGuard} from './theme/utility/auth-guard.service';
import { LayoutComponent } from './theme/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    loadChildren: () => import('./apps/authentication/authentication.module').then(module => module.AuthenticationModule)
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./apps/dashboard/dashboard.module').then(module => module.DashboardModule)
      },
      {
        path: 'doc',
        loadChildren: () => import('./apps/doc/doctor.module').then(module => module.DoctorModule)
      },
      {
        path: 'referal',
        loadChildren: () => import('./apps/referal/referal.module').then(module => module.ReferalModule)
      },
      {
        path: 'offeredservices',
        loadChildren: () => import('./apps/offeredServices/offeredServices.module').then(module => module.OfferedServicesModule)
      },
      {
        path: 'order',
        loadChildren: () => import('./apps/order/order.module').then(module => module.OrderModule)
      },
      {
        path: 'quickPayLink',
        loadChildren: () => import('./apps/quickPay/quickPay.module').then(module => module.QuickPayModule)
      },
      {
        path: 'doctorReferal',
        loadChildren: () => import('./apps/docReferalProgram/docReferalProgram.module').then(module => module.DocRefProgramModule)
      },
      {
        path: 'sample-page',
        loadChildren: () => import('./apps/sample-page/sample-page.module').then(module => module.SamplePageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
    enableTracing: false, // <-- debugging purposes only
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
