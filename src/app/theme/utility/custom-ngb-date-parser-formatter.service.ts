import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DATE_FORMAT } from 'src/app/app-constants';

export class CustomNgbDateParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        if (value) {
            value = value.trim();
            let mdt = moment(value, DATE_FORMAT);
        }
        return null;
    }
    format(date: NgbDateStruct): string {
        if (!date) {
            return '';
        }
        let mdt = moment([date.year, date.month - 1, date.day]);
        if (!mdt.isValid()) {
            return '';
        }
        return mdt.format(DATE_FORMAT);
    }
}
