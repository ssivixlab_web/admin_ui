import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_END_POINT_URL, SESSION_STORAGE_KEY } from 'src/app/app-constants';

@Injectable({
    providedIn: 'root'
})
export class AppHttpApiService {
    private endPoint = API_END_POINT_URL;
    private customReqOptions: HttpHeaders = null;

    constructor( private httpClient: HttpClient) {}

    private getHeaders( headers?: HttpHeaders): HttpHeaders {
        if (!headers) {
            headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            headers = headers.append('Access-Control-Allow-Headers', 'Content-Type');
            headers = headers.append('Access-Control-Allow-Methods', '*');
            headers = headers.append('Access-Control-Allow-Origin', '*');
            headers = headers.append('x-access-token', sessionStorage.getItem(SESSION_STORAGE_KEY.TOKEN) ? sessionStorage.getItem(SESSION_STORAGE_KEY.TOKEN): "" );
        }
        return headers;
    }

    public getDataTest<T>(url: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.get(`${url}`, { headers: this.customReqOptions});
    }

    public getData(url: string, headers?: HttpHeaders): Observable<any>;
    public getData<T>(url: string, headers?: HttpHeaders): Observable<T>;
    public getData<T>(url: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.get(`${this.endPoint}${url}`, { headers: this.customReqOptions});
    }

    public postData(url: string, payload: any, headers?: HttpHeaders): Observable<any>;
    public postData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T>;
    public postData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.post(`${this.endPoint}${url}`, payload, { headers: this.customReqOptions} );
    }

    public putData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.put(`${this.endPoint}${url}`, payload , { headers: this.customReqOptions});
    }

    public deleteData<T>(url: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.delete(`${this.endPoint}${url}`, { headers: this.customReqOptions});
    }
}
