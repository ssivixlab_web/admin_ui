export class DateUtility {
    updateStartandEndDate(payload, startDateKey: string = 'startDate', endDatekey: string = 'EndDate'): any {
        let newPayload = {...payload};
        let sDt = ''; let eDt = '';
        if (newPayload[startDateKey].includes('T')) {
            sDt  = newPayload[startDateKey].split('T')[0];
        } else {
            sDt  = newPayload[startDateKey];
        }
        newPayload[startDateKey] = `${sDt}T00:00:00.000Z`;

        if (newPayload[endDatekey].includes('T')) {
            eDt  = newPayload[endDatekey].split('T')[0];
        } else {
            eDt  = newPayload[endDatekey];
        }
        newPayload[endDatekey] = `${eDt}T23:59:00.000Z`;

        return newPayload;
    }
}
