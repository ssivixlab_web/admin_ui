import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppAuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AppAuthGuardService implements CanActivate {
  constructor(public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!AppAuthenticationService.isAuthenticated()) {
      this.router.navigate(['auth/signin']);
      return false;
    }
    return true;
  }
}
