import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import { UiModalComponent } from 'src/app/theme/shared/components/modal/ui-modal/ui-modal.component';
import { Router } from '@angular/router';
import { AppAuthenticationService } from 'src/app/theme/utility/authentication.service';

@Component({
  selector: 'app-nav-right',
  templateUrl: './nav-right.component.html',
  styleUrls: ['./nav-right.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavRightComponent implements OnInit {
  @ViewChild('signOutModal', {static: true}) signOutModalRef: UiModalComponent;

  constructor(private router: Router) { }

  ngOnInit() { }

  confirmSignOut(): void {
    this.signOutModalRef.show();
  }

  close(redirect = false): void {
    this.signOutModalRef.hide();
    if (redirect) {
      AppAuthenticationService.removeSessionStorage();
      this.router.navigateByUrl('');
    }
  }
}
