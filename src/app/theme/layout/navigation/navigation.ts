import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather-monitor',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        url: '/dashboard/analytics',
        icon: 'feather-home'
      },
      // {
      //   id: 'page-layouts',
      //   title: 'Horizontal Layouts',
      //   type: 'item',
      //   url: '/layout/horizontal',
      //   target: true,
      //   icon: 'feather-layout'
      // }
    ]
  },
  {
    id: 'doc',
    title: 'Medical Appointment',
    type: 'group',
    icon: 'feather-layers',
    children: [
      {
        id: 'medicalOverview',
        title: 'Overview Dashboard',
        type: 'item',
        url: '/doc/overview',
        icon: 'feather-box',
      },
      {
        id: 'forms-upload',
        title: 'Bulk Upload',
        type: 'item',
        url: '/doc/bulk-upload',
        icon: 'feather-file-text'
      },
      {
        id: 'forms-upload',
        title: 'Article Upload',
        type: 'item',
        url: '/doc/article-upload',
        icon: 'feather-file-text'
      },
      {
        id: 'manage',
        title: 'Search and Manage',
        type: 'item',
        url: '/doc/search',
        icon: 'feather-search'
      }
    ]
  },
  {
    id: 'referal',
    title: 'Referal',
    type: 'group',
    children: [
      {
        id: 'ref',
        title: 'Referal Code Manage',
        type: 'item',
        url: '/referal',
        icon: 'feather-gift'
      },
      {
        id: 'offeredServices',
        title: 'Product & Service',
        type: 'item',
        url: '/offeredservices',
        icon: 'feather-gift'
      },
      {
        id: 'order',
        title: 'Order',
        type: 'item',
        url: '/order',
        icon: 'feather-gift'
      },
      {
        id: 'pay',
        title: 'Fast Payment',
        type: 'item',
        url: '/quickPayLink',
        icon: 'feather-credit-card'
      },
      {
        id: 'docRefPro',
        title: 'Doctor Referal Program',
        type: 'item',
        url: '/doctorReferal',
        icon: 'feather-users'
      }
    ]
  },
  {
    id: 'other',
    title: 'Other',
    type: 'group',
    icon: 'feather-align-left',
    children: [
      // {
      //   id: 'menu-level',
      //   title: 'Menu Levels',
      //   type: 'collapse',
      //   icon: 'feather-menu',
      //   children: [
      //     {
      //       id: 'menu-level-2.1',
      //       title: 'Menu Level 2.1',
      //       type: 'item',
      //       url: 'javascript:',
      //       external: true
      //     },
      //     {
      //       id: 'menu-level-2.2',
      //       title: 'Menu Level 2.2',
      //       type: 'collapse',
      //       children: [
      //         {
      //           id: 'menu-level-2.2.1',
      //           title: 'Menu Level 2.2.1',
      //           type: 'item',
      //           url: 'javascript:',
      //           external: true
      //         },
      //         {
      //           id: 'menu-level-2.2.2',
      //           title: 'Menu Level 2.2.2',
      //           type: 'item',
      //           url: 'javascript:',
      //           external: true
      //         }
      //       ]
      //     }
      //   ]
      // },
      // {
      //   id: 'disabled-menu',
      //   title: 'Disabled Menu',
      //   type: 'item',
      //   url: 'javascript:',
      //   classes: 'nav-item disabled',
      //   icon: 'feather-power',
      //   external: true
      // },
      {
        id: 'sample-page',
        title: 'Sample Page',
        type: 'item',
        url: '/sample-page',
        classes: 'nav-item',
        icon: 'feather-sidebar'
      }
    ]
  }
];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
