import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlertModule, BreadcrumbModule, CardModule, ModalModule } from './components';
import { DataFilterPipe } from './components/data-table/data-filter.pipe';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ClickOutsideModule } from 'ng-click-outside';

import { SpinnerComponent } from './components/spinner/spinner.component';
import { NgbModule, NgbDateParserFormatter, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { SafePipe } from '../utility/safe.pipe';
import { AppNumberOnlyDirective } from '../utility/number-only.directive';
import { DateUtility } from '../utility/dateUtility.service';
import { CustomNgbDateParserFormatter } from '../utility/custom-ngb-date-parser-formatter.service';
import { CustomNgbDateAdapter } from '../utility/custom-ngb-date-adapter.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerService } from './components/spinner/spinner.service';
import { SpinnerHTTPInterceptor } from './components/spinner/spinner-http.interceptor.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule,
    CardModule,
    BreadcrumbModule,
    ModalModule,
    ClickOutsideModule,
    NgbModule,
  ],
  exports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule,
    CardModule,
    BreadcrumbModule,
    ModalModule,
    DataFilterPipe,
    ClickOutsideModule,
    SpinnerComponent,
    NgbModule,
    SafePipe,
    AppNumberOnlyDirective
  ],
  declarations: [
    DataFilterPipe,
    SpinnerComponent,
    SafePipe,
    AppNumberOnlyDirective
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: NgbDateParserFormatter,
      useClass: CustomNgbDateParserFormatter
    },
    { provide: NgbDateAdapter,
      useClass: CustomNgbDateAdapter
    },
    DateUtility,
    CustomNgbDateParserFormatter,
    CustomNgbDateAdapter,
    SpinnerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerHTTPInterceptor,
      multi: true
    }
  ]
})
export class SharedModule { }
