import {Component, Input, OnInit, ViewEncapsulation, ViewChild, Output, EventEmitter} from '@angular/core';
import { UiModalComponent } from '../ui-modal/ui-modal.component';

@Component({
  selector: 'app-animation-modal',
  templateUrl: './animation-modal.component.html',
  styleUrls: ['./animation-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AnimationModalComponent extends UiModalComponent {
  @ViewChild('msgModal', {static: true}) msgModalRef: UiModalComponent;
  @Input() modalMsg: string;
  @Output() okClickEmit: EventEmitter<any> = new EventEmitter();

  constructor() {
    super();
  }

  closeAndRefresh() {
    this.okClickEmit.emit({modalClose: true});
    this.msgModalRef.hide();
  }
  public showSuccessModal() {
    this.msgModalRef.show();
  }

}
