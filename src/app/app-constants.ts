import { environment } from 'src/environments/environment';
export const API_END_POINT_URL = environment.endPointUrl;
// All get/post/put/delete End point
export const API_URL = {
    SIGNIN: 'partnerapp/api/auth/signin',
    BULK_UPLOAD: 'doctor/profile/upload',
    BLOG_UPLOAD:'article/upload',
    BLOG_LIST:'article/list',
    TOTAL_DOC_COUNT: 'doctor/profile/totalDocCount',
    DOC_STATS_VIEW: 'doctor/profile/topDocList',
    DOCTOR_SEARCH: 'doctor/list',
    PROFILE: 'doctor/profile', // EDIT DELETE
    PROFILE_ADD: 'doctor/profile/add',
    REFERAL_CODE_LIST: 'company/referral/code/list',
    REFERAL_CODE_CHANGE: 'company/referral/code/newOrEdit',
    OFFERED_SERVICES_CODE_LIST: 'api/offeredservice/getall',
    OFFERED_SERVICES_CODE_CHANGE: 'api/offeredservice/update/',
    ORDER_CODE_LIST: 'api/order/getall',
    ORDER_CODE_CHANGE: 'api/order/update/',
    OFFERED_SERVICES_ADD: 'api/offeredservice/add',
    QUICK_PAY_LIST: 'pay/payment/details/all',
    QUICK_PAY_Add: 'pay/details',
    DOC_REF_PRO_LIST: 'doctor/refer/all',
};

// Base/Configured data
export const STATIC_BASE_API_URL = {
};

// Application Constants
/// TODO REMOVE -- untill API is not their
export const SIGNIN_VALUE = {
    EMAIL_ID: 'ssivixlab.dev@gmail.com',
    PASSWORD: 'carewell',
    LOGEDIN_USERID: Math.random().toString(36).substr(2, 9),
    USER: '{name: SSivixLab Admin}'
};


// Session, Header, Localstorage related constants
export const SESSION_STORAGE_KEY = {
    TOKEN : 'SSIVIXLAB_ADMIN_authtoken',
    USER_DATA: 'SSIVIXLAB_ADMIN_user_data',
    USER_NAME: 'SSIVIXLAB_ADMIN_user_name',
};

export const VALIDATION = {
};

export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_FORMAT_UI = 'yyyy-MM-dd';
