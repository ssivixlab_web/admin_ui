import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderService } from './order.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UiModalComponent } from 'src/app/theme/shared/components/modal/ui-modal/ui-modal.component';
import { NgbDateStruct, NgbCalendar, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { IOrderResponse, IOrder, IProduct } from './order';
import { AnimationModalComponent } from 'src/app/theme/shared/components/modal/animation-modal/animation-modal.component';
import { CustomNgbDateAdapter } from 'src/app/theme/utility/custom-ngb-date-adapter.service';
import { DateUtility } from 'src/app/theme/utility/dateUtility.service';
import { DATE_FORMAT_UI, SESSION_STORAGE_KEY } from 'src/app/app-constants';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  @ViewChild('orderModal', { static: true }) orderModalRef: UiModalComponent;
  @ViewChild('msgModal', { static: true }) msgModalRef: AnimationModalComponent;
  msg: string = '';
  dateformatUI = DATE_FORMAT_UI;
  searchResultData: IOrder[] = [];
  productData: IProduct[] = [];
  addEditOrderFb: FormGroup;
  productFb: FormGroup;
  constructor(private refCodeService: OrderService,
    private fb: FormBuilder,
    private dt: CustomNgbDateAdapter,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>,
    private dateutil: DateUtility) {
    this.addEditOrderFb = this.fb.group({
      customercountrycode: new FormControl('', [Validators.required]),
      customerphone: new FormControl('', [Validators.required]),
      customeraddress: new FormControl('', [Validators.required]),
      customercountry: new FormControl('', [Validators.required]),
      orderstatus: new FormControl('', [Validators.required]),
      totalamount: new FormControl('', [Validators.required]),
      paymentstatus: new FormControl('', [Validators.required]),
      currency: new FormControl('', [Validators.required]),
      transectionid: new FormControl(),
      remarks: new FormControl('', [Validators.required]),
      sourceip: new FormControl('', [Validators.required]),
      _id: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      customername: new FormControl('', [Validators.required]),
      customeremail: new FormControl('', [Validators.required]),
      productlist: new FormControl('', [Validators.required]),
      orderdt: new FormControl('', [Validators.required]),
      paymentdt: new FormControl(''),
      patientname:new FormControl(''),
      patientemail:new FormControl(''),
      patientzipcode:new FormControl(''),
      patientcountrycode:new FormControl(''),
      patientphone:new FormControl(''),
      patientaddress:new FormControl(''),
      patientcountry:new FormControl(''),
      patienttestDate:new FormControl(''),
      formurl: new FormControl()
    });
    this.productFb = this.fb.group({
      productname: new FormControl('', [Validators.required]),
      productprice: new FormControl('', [Validators.required]),
      productcurrency: new FormControl('', [Validators.required]),
      productimage: new FormControl('', [Validators.required]),
      productid: new FormControl('', [Validators.required])   
    })
  }

  ngOnInit() {
    this.getOrderList();
  }

  getOrderList() {
    this.refCodeService.getOrderCodeList().subscribe((res: IOrderResponse) => {
      this.searchResultData = res.data;
    });
  }

  showCardFooter() {
    return this.searchResultData.length > 0 ? false : true;
  }

  openModal() {
    this.addEditOrderFb.reset();
    if (this.addEditOrderFb.get('_id') != null) {
      this.addEditOrderFb.removeControl('_id');
    }
    this.orderModalRef.show();
  }

  refreshnow($ev) {
    this.getOrderList();
  }

  closeModal() {
    this.orderModalRef.hide();
  }

  edit(obj: IOrder) {
    this.openModal();
    this.addEditOrderFb.addControl('_id', new FormControl());
    this.addEditOrderFb.patchValue(obj);
    this.productData = JSON.parse(obj.productlist.split("'").join("\""))
  }

  delete(refId) {
    let delRef = {
      _id: refId,
      isActive: false
    };
    this.refCodeService.editOrder(delRef).subscribe((res) => {
      this.msg = 'Is InActive';
      this.msgModalRef.showSuccessModal();
    }, (err) => {
      console.log('err', err);
    });
  }

  submit(payload: IOrder) {
    // if (payload.limit === '') {
    //   payload = {...payload};
    //   payload.limit = null;

    // }
    // payload = this.dateutil.updateStartandEndDate(payload);
    // var token = sessionStorage.getItem(SESSION_STORAGE_KEY.TOKEN)
    // console.log("Token", payload);
    if (payload._id) {
      this.refCodeService.editOrder(payload).subscribe((res) => {
        console.log("Update", payload._id)

        this.msg = res.message;
        this.closeModal();
        this.msgModalRef.showSuccessModal();
      }, (err) => {
        console.log('err', err);
      });
    }
    //  else {
    //   this.refCodeService.addOrder(payload).subscribe((res) => {
    //     console.log("Create", payload.productavailablity)

    //     this.msg = res.message;
    //     this.closeModal();
    //     this.msgModalRef.showSuccessModal();
    //   }, (err) => {
    //     console.log('err', err);
    //   });
    // }

  }

  getToday() {
    if (this.addEditOrderFb.get('_id') == null) {
      return this.dt.fromModel(this.dateAdapter.toModel(this.ngbCalendar.getToday()));
    }
  }

  getMaxDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditOrderFb.get('EndDate').value);
  }

  getMinDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditOrderFb.get('startDate').value);
  }
}
