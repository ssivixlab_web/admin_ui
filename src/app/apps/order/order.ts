export interface IOrderResponse {
    data: IOrder[];
    status: boolean;
}

export interface IOrderOpRes {
    data: string;
    status: boolean;
    message: string;
}

export interface IOrder {
    customercountrycode: string,
    customerphone: string,
    customeraddress: string,
    customercountry: string,
    orderstatus: string,
    totalamount: number,
    paymentstatus: boolean,
    currency: string,
    transectionid: string,
    remarks: string,
    sourceip: string,
    _id: string,
    name: string,
    customername: string,
    customeremail: string,
    productlist: string,
    orderdt: string,
    paymentdt: string,
    patientname:string,
    patientemail:string,
    patientzipcode:string,
    patientcountrycode:string,
    patientphone:number,
    patientaddress:string,
    patientcountry:string,
    patienttestDate:string,
    formurl: string
}

export interface IProduct {
    productname: string,
    productprice: string,
    productcurrency: string,
    productimage: string,
    productid: string   
}
