
import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { Observable, of } from 'rxjs';
import { IOrderResponse, IOrderOpRes } from './order';

@Injectable()
export class OrderService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getOrderCodeList(): Observable<IOrderResponse> {
        return this.httpApi.getData(`${API_URL.ORDER_CODE_LIST}`);
    }

    editOrder(payload): Observable<IOrderOpRes> {
        return this.httpApi.putData(`${API_URL.ORDER_CODE_CHANGE}` + payload.name, payload);
    }

    // addOrder(payload): Observable<IOrderOpRes> {
    //     return this.httpApi.postData(`${API_URL.OFFERED_SERVICES_ADD}`, payload);
    // }

}
