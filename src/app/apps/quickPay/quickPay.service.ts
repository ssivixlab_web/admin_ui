
import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { Observable, of } from 'rxjs';

@Injectable()
export class QuickPayService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getQuickPayList(): Observable<any> {
        return this.httpApi.getData(`${API_URL.QUICK_PAY_LIST}`);
    }

    addQuickPay(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.QUICK_PAY_Add}`, payload);
    }

}
