import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { QuickPayService } from './quickPay.service'; 

@Component({
  templateUrl: './quickPay.component.html',
  styleUrls: ['./quickPay.component.scss']
})
export class QuickPayComponent implements OnInit {
  createQuickPayFb: FormGroup;
  searchResultData: any = [];
  apisuccesss: boolean = false;
  amoutPreFix: string = 'S$';
  countryCodePreFix: string = '+65';
  private errorMsg = {
    required: 'This field is Required',
    email: 'Invalid Email',
    minlength: 'Invalid Mobile Number',
    maxlength: 'Invalid Mobile Number',
    numCheck: 'Amount Should be greater than 0'
  };

  constructor(private fb: FormBuilder,
              private quickpay: QuickPayService) { }

  ngOnInit() {
    this.generateFb();
    this.getListOfQuickPay();
  }

  private numCheckValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && (isNaN(control.value) || control.value < 1 )) {
        return { numCheck: true };
    }
    return null;
  }

  private generateFb() {
    this.createQuickPayFb = this.fb.group({
      paymentDescription: ['', Validators.required],
      emailId: ['', [Validators.required, Validators.email]],
      mobileNumber: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
      amount: ['', [Validators.required, this.numCheckValidator]],
      countryCode: [this.countryCodePreFix]
    });
  }

  private getListOfQuickPay() {
    this.quickpay.getQuickPayList().subscribe(res => {
      if (res.status && res.data) {
        this.searchResultData = res.data;
      }
    });
  }

  submit(payload) {
    this.quickpay.addQuickPay(payload).subscribe((res) => {
      if (res.status) {
        this.apisuccesss = true;
        this.createQuickPayFb.reset({
          countryCode: this.countryCodePreFix
        });
        this.getListOfQuickPay();
      }
    });
  }

  getError(formcontrol: string): string {
    const fc: AbstractControl = this.createQuickPayFb.get(`${formcontrol}`);
    if(fc.errors && fc.touched) {
      let error: string ='';
      for(const [key, value] of Object.entries(fc.errors)) {
        error = this.errorMsg[key];
      }
      return error;
    }
  }

}
