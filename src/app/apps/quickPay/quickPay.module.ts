import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../theme/shared/shared.module';

import { QuickPayComponent } from './quickPay.component';
import { RouterModule, Routes } from '@angular/router';
import { QuickPayService } from './quickPay.service';

const routes: Routes = [
  {
    path: '',
    component: QuickPayComponent
  }
];
@NgModule({
  declarations: [QuickPayComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    QuickPayService
  ]
})
export class QuickPayModule { }
