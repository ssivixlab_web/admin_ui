import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import {SharedModule} from '../../theme/shared/shared.module';
import { DashbordService } from './dashboard.service';
import { AppHttpApiService } from '../../../app/theme/utility/http-api.service';
import { AppAuthGuardService } from '../../../app/theme/utility/auth-guard.service';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ],
  providers: [DashbordService],
})
export class DashboardModule { }
