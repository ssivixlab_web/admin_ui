import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class DashbordService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getTest() {
       return this.httpApi.getDataTest('https://jsonplaceholder.typicode.com/todos/');
    }
}
