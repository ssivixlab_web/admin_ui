export interface IOfferedServicesResponse {
    data: IOfferedServices[];
    status: boolean;
}

export interface IOfferedServicesOpRes {
    data: string;
    status: boolean;
    message: string;
}

export interface IOfferedServices {
    productavailablity: boolean,
    producturl: string,
    productdescription: string,
    productimage: string,
    producttype: string,
    currency: string,
    price: Number,
    country: string,
    sku: string,
    vendor: string,
    _id: string,
    productname: string,
    tag: string,
    onbardingon: string,
    order : Number,
    appointment : boolean,
    formurl : string
}
