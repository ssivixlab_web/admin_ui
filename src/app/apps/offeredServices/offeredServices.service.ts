
import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { Observable, of } from 'rxjs';
import { IOfferedServicesResponse, IOfferedServicesOpRes } from './offeredServices';

@Injectable()
export class OfferedServicesService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getOfferedServicesCodeList(): Observable<IOfferedServicesResponse> {
        return this.httpApi.getData(`${API_URL.OFFERED_SERVICES_CODE_LIST}`);
    }

    editOfferedServices(payload): Observable<IOfferedServicesOpRes> {
        return this.httpApi.putData(`${API_URL.OFFERED_SERVICES_CODE_CHANGE}` + payload.sku, payload);
    }

    addOfferedServices(payload): Observable<IOfferedServicesOpRes> {
        return this.httpApi.postData(`${API_URL.OFFERED_SERVICES_ADD}`, payload);
    }

}
