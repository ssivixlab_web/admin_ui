import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OfferedServicesComponent} from './offeredServices.component';

const routes: Routes = [
  {
    path: '',
    component: OfferedServicesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferedServicesRoutingModule { }
