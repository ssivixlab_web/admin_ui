import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferedServicesRoutingModule } from './offeredServices-routing.module';
import { OfferedServicesComponent } from './offeredServices.component';
import {SharedModule} from '../../theme/shared/shared.module';
import { OfferedServicesService } from './offeredServices.service';

@NgModule({
  declarations: [OfferedServicesComponent],
  imports: [
    CommonModule,
    OfferedServicesRoutingModule,
    SharedModule
  ],
  providers: [
    OfferedServicesService
  ]
})
export class OfferedServicesModule { }
