import { Component, OnInit, ViewChild } from '@angular/core';
import { OfferedServicesService } from './offeredServices.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UiModalComponent } from 'src/app/theme/shared/components/modal/ui-modal/ui-modal.component';
import { NgbDateStruct, NgbCalendar, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { IOfferedServicesResponse, IOfferedServices } from './offeredServices';
import { AnimationModalComponent } from 'src/app/theme/shared/components/modal/animation-modal/animation-modal.component';
import { CustomNgbDateAdapter } from 'src/app/theme/utility/custom-ngb-date-adapter.service';
import { DateUtility } from 'src/app/theme/utility/dateUtility.service';
import { DATE_FORMAT_UI, SESSION_STORAGE_KEY } from 'src/app/app-constants';

@Component({
  selector: 'app-offeredServices',
  templateUrl: './offeredServices.component.html',
  styleUrls: ['./offeredServices.component.scss']
})
export class OfferedServicesComponent implements OnInit {
  @ViewChild('offeredServicesModal', { static: true }) offeredServicesModalRef: UiModalComponent;
  @ViewChild('msgModal', { static: true }) msgModalRef: AnimationModalComponent;
  msg: string = '';
  dateformatUI = DATE_FORMAT_UI;
  searchResultData: IOfferedServices[] = [];
  addEditOfferedServicesFb: FormGroup;
  constructor(private refCodeService: OfferedServicesService,
    private fb: FormBuilder,
    private dt: CustomNgbDateAdapter,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>,
    private dateutil: DateUtility) {
    this.addEditOfferedServicesFb = this.fb.group({
      productavailablity: new FormControl('', [Validators.required]),
      producturl: new FormControl('', [Validators.required]),
      productdescription: new FormControl('', [Validators.required]),
      productimage: new FormControl('', [Validators.required]),
      producttype: new FormControl('', [Validators.required]),
      currency: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
      sku: new FormControl('', [Validators.required]),
      vendor: new FormControl('', [Validators.required]),
      productname: new FormControl('', [Validators.required]),
      tag: new FormControl('', [Validators.required]),
      order: new FormControl('', [Validators.required]),
      appointment: new FormControl('', [Validators.required]),
      formurl: new FormControl()
    });
  }

  ngOnInit() {
    this.getOfferedServicesList();
  }

  getOfferedServicesList() {
    this.refCodeService.getOfferedServicesCodeList().subscribe((res: IOfferedServicesResponse) => {
      this.searchResultData = res.data;
    });
  }

  showCardFooter() {
    return this.searchResultData.length > 0 ? false : true;
  }

  openModal() {
    this.addEditOfferedServicesFb.reset();
    if (this.addEditOfferedServicesFb.get('_id') != null) {
      this.addEditOfferedServicesFb.removeControl('_id');
    }
    this.offeredServicesModalRef.show();
  }

  refreshnow($ev) {
    this.getOfferedServicesList();
  }

  closeModal() {
    this.offeredServicesModalRef.hide();
  }

  edit(obj: IOfferedServices) {
    this.openModal();
    this.addEditOfferedServicesFb.addControl('_id', new FormControl());
    this.addEditOfferedServicesFb.patchValue(obj);
  }

  delete(refId) {
    let delRef = {
      _id: refId,
      isActive: false
    };
    this.refCodeService.editOfferedServices(delRef).subscribe((res) => {
      this.msg = 'Is InActive';
      this.msgModalRef.showSuccessModal();
    }, (err) => {
      console.log('err', err);
    });
  }

  submit(payload: IOfferedServices) {
    // if (payload.limit === '') {
    //   payload = {...payload};
    //   payload.limit = null;

    // }
    // payload = this.dateutil.updateStartandEndDate(payload);
    // var token = sessionStorage.getItem(SESSION_STORAGE_KEY.TOKEN)
    // console.log("Token", payload);
    if (payload._id) {
      this.refCodeService.editOfferedServices(payload).subscribe((res) => {
        console.log("Update", payload._id)

        this.msg = res.message;
        this.closeModal();
        this.msgModalRef.showSuccessModal();
      }, (err) => {
        console.log('err', err);
      });
    } else {
      this.refCodeService.addOfferedServices(payload).subscribe((res) => {
        console.log("Create", payload.productavailablity)

        this.msg = res.message;
        this.closeModal();
        this.msgModalRef.showSuccessModal();
      }, (err) => {
        console.log('err', err);
      });
    }

  }

  getToday() {
    if (this.addEditOfferedServicesFb.get('_id') == null) {
      return this.dt.fromModel(this.dateAdapter.toModel(this.ngbCalendar.getToday()));
    }
  }

  getMaxDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditOfferedServicesFb.get('EndDate').value);
  }

  getMinDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditOfferedServicesFb.get('startDate').value);
  }
}
