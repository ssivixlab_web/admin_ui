import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../theme/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { DocReferalProgramComponent } from './docReferalProgram.component';
import { DocReferalProgramService } from './docReferalProgram.service';

const routes: Routes = [
  {
    path: '',
    component: DocReferalProgramComponent
  }
];
@NgModule({
  declarations: [DocReferalProgramComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    DocReferalProgramService
  ]
})
export class DocRefProgramModule { }
