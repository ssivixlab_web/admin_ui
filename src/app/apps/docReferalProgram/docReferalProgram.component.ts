import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DocReferalProgramService } from './docReferalProgram.service';

@Component({
  templateUrl: './docReferalProgram.component.html',
  styleUrls: ['./docReferalProgram.component.scss']
})
export class DocReferalProgramComponent implements OnInit, OnDestroy {
  docRefProgData: any = [];
  subscribtion: Subscription = new Subscription();
  constructor(private docRefService: DocReferalProgramService) { }

  ngOnInit() {
    this.getDocReferalProgramList();
  }

  private getDocReferalProgramList() {
    this.subscribtion.add(this.docRefService.getDocRefProList().subscribe(res => {
      if (res.status && res.data) {
        this.docRefProgData = res.data;
      }
    }));
  }

  ngOnDestroy() {
    this.subscribtion.unsubscribe();
  }


}
