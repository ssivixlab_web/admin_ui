
import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { Observable, of } from 'rxjs';

@Injectable()
export class DocReferalProgramService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getDocRefProList(): Observable<any> {
        return this.httpApi.getData(`${API_URL.DOC_REF_PRO_LIST}`);
    }


}
