import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DoctorService {
    constructor(private httpApi: AppHttpApiService) {
    }

    bulkUpload(fileToUpload: File) {
        const formData: FormData = new FormData();
        formData.append('recFile', fileToUpload);
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        return this.httpApi.postData(API_URL.BULK_UPLOAD , formData, HttpUploadOptions.headers);
    }

    blogUpload(fileToUpload: File) {
        const formData: FormData = new FormData();
        formData.append('article', fileToUpload);
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        return this.httpApi.postData(API_URL.BLOG_UPLOAD , formData, HttpUploadOptions.headers);
    }

    getBlogList(): Observable<any> {
        return this.httpApi.postData(API_URL.BLOG_LIST, {});
    }

    getDoctorList(filterData): Observable<any> {
        return this.httpApi.postData(API_URL.DOCTOR_SEARCH, {
            searchText: filterData.searchText,
            pageNo: filterData.pageNo,
            size: 10
        });
    }

    getDocCount(): Observable<any> {
        return this.httpApi.postData(API_URL.TOTAL_DOC_COUNT, {});
    }

    getStats(payload): Observable<any> {
        return this.httpApi.postData(API_URL.DOC_STATS_VIEW, payload);
    }

    addDoctor(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.PROFILE_ADD}`, payload);
    }

    editDoctor(payload, doctorId): Observable<any> {
        return this.httpApi.putData(`${API_URL.PROFILE}/${doctorId}`, payload);
    }

    deleteDoctor(doctorId): Observable<any> {
        return this.httpApi.deleteData(`${API_URL.PROFILE}/${doctorId}`);
    }

}
