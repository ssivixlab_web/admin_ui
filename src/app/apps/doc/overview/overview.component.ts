import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../doctor.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbCalendar, NgbDateAdapter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { CustomNgbDateAdapter } from 'src/app/theme/utility/custom-ngb-date-adapter.service';
import { CustomNgbDateParserFormatter } from 'src/app/theme/utility/custom-ngb-date-parser-formatter.service';
import { DATE_FORMAT } from 'src/app/app-constants';
import { DateUtility } from 'src/app/theme/utility/dateUtility.service';

@Component({
  selector: 'app-doc-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class DocOverviewComponent implements OnInit {

  totalCount: number;
  filterResult = [];
  filterDocStatsFb: FormGroup;
  timeFilter = '';
  constructor(public docService: DoctorService,
              private fb: FormBuilder,
              private dt: CustomNgbDateAdapter,
              private ngbCalendar: NgbCalendar,
              private dateAdapter: NgbDateAdapter<string>,
              private formatDt: CustomNgbDateParserFormatter,
              private dateutil: DateUtility) {
    this.filterDocStatsFb = this.fb.group({
      sortBy: new FormControl('sortByProfileVisit', [Validators.required]),
      startDate: new FormControl('', [Validators.required]),
      endDate: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    this.docService.getDocCount().subscribe((res) => {
      this.totalCount = res.totalCount;
    });
  }

  getToday() {
    return this.dt.fromModel(this.dateAdapter.toModel(this.ngbCalendar.getToday()));
  }

  getMaxDate(): NgbDateStruct {
    return this.dt.fromModel(this.filterDocStatsFb.get('endDate').value);
  }

  getMinDate(): NgbDateStruct {
    return this.dt.fromModel(this.filterDocStatsFb.get('startDate').value);
  }

  timeFilterChanged($ev) {
    let startdt = this.formatDt.format(this.getToday());
    switch ($ev) {
      case '':
      case 'today':
        this.filterDocStatsFb.get('startDate').setValue(startdt);
        this.filterDocStatsFb.get('endDate').setValue(startdt);
        break;
      case 'day':
      case 'week':
      case 'month':
        const endDt = moment().subtract(1, $ev).format(DATE_FORMAT);
        this.filterDocStatsFb.get('startDate').setValue(endDt);
        this.filterDocStatsFb.get('endDate').setValue(startdt);
        break;
      case 'customRange':
        this.filterDocStatsFb.get('startDate').setValue(null);
        this.filterDocStatsFb.get('endDate').setValue(null);
        break;
      default:
        break;
    }
  }

  filterReport(payload) {
    let payloadnew = {...payload};
    payloadnew = this.dateutil.updateStartandEndDate(payloadnew, 'startDate', 'endDate');
    this.docService.getStats(payloadnew).subscribe((res) => {
      if (res.status && res.topDocList) {
        this.filterResult = res.topDocList;
      }
    });
  }

  getDoc(item) {
    return item.doctorDetails && item.doctorDetails[0] ? item.doctorDetails[0] : {};
  }

}
