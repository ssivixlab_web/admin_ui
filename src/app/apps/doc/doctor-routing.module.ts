import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocOverviewComponent } from './overview/overview.component';
import { DocBulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { DocSearchComponent } from './search/search.component';
import { ArticleUploadComponent } from './article-upload/article-upload.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'overview',
        component: DocOverviewComponent
      },
      {
        path: 'bulk-upload',
        component: DocBulkUploadComponent
      },
      {
        path: 'search',
        component: DocSearchComponent
      },
      {
        path: 'article-upload',
        component: ArticleUploadComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
