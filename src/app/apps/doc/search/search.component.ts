import { Component, OnInit, ViewChild } from '@angular/core';
import { DoctorService } from '../doctor.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AnimationModalComponent } from 'src/app/theme/shared/components/modal/animation-modal/animation-modal.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-doc-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class DocSearchComponent implements OnInit {
  @ViewChild('msgModal', {static: true}) msgModalRef: AnimationModalComponent;
  parentSubject: Subject<any> = new Subject();
  searchDoctorFb: FormGroup;
  searchResultData = [];
  msg = '';
  pageConfig = {
    itemsPerPage: 10,
    page: 1,
    totalItems: 0
  };

  constructor(public docService: DoctorService, private fb: FormBuilder) {
    this.searchDoctorFb = this.fb.group({
      searchText: ['', Validators.required],
      pageNo: [1, Validators.required]
    });
  }

  ngOnInit() {
    this.searchDoctor();
  }
  // Search Module----------
  search() {
    this.searchDoctorFb.get('pageNo').setValue(1);
    this.searchDoctor();
  }

  reset() {
    this.searchDoctorFb.reset();
    this.searchDoctor();
  }

  searchDoctor() {
    this.docService.getDoctorList(this.searchDoctorFb.getRawValue()).subscribe((res)=> {
      this.searchResultData = res.data;
      this.pageConfig.totalItems = res.totalPage * 10;
    }, (err) => {
      console.log('err', err);
    });
  }

  pageChange(pageNo) {
    this.searchDoctorFb.get('pageNo').setValue(pageNo);
    this.searchDoctor();
  }

  addBtnClick() {
    this.parentSubject.next();
  }

  editBtnClick(doctor) {
    this.parentSubject.next({
      docObj: doctor
    });
  }

  delBtnClick(doc) {
    this.docService.deleteDoctor(doc._id).subscribe((res) => {
      this.clickBtnHandler(res.success);
    });
  }
  refreshnow($ev) {
    this.searchDoctor();
  }

  clickBtnHandler($ev) {
    this.msg = $ev;
    this.msgModalRef.showSuccessModal();
  }
}
