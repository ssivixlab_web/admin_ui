import { Component, OnInit, ViewChild, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { UiModalComponent } from 'src/app/theme/shared/components/modal/ui-modal/ui-modal.component';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { DoctorService } from '../../doctor.service';
import { Subject } from 'rxjs';
import { GenericValidator } from 'src/app/theme/utility/generic.validators';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-doc-add-edit',
    templateUrl: './addEdit.component.html',
    styleUrls: ['./addEdit.component.scss']
})

export class DocAddEditComponent implements OnInit {
    @Input() parentSubject: Subject<any>;
    @Output() okClickBtn: EventEmitter<any> = new EventEmitter();
    @ViewChild('myPersistenceModal', {static: true}) nameInputRef: UiModalComponent;
    addEditDocFb: FormGroup;
    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    constructor(public docService: DoctorService, private fb: FormBuilder) {
        this.addFormGenerate();
        this.validationMessages = {
            name: {
              required: 'Name is required'
            },
            contactNumber: {
              required: 'Contact Number is required'
            },
            nameOfClinic: {
                required: 'Please enter Clinic Name'
            },
            clinicAddress: {
                required: 'Clinic Address is required'
            },
            gender: {
                required: 'Please select the Gender'
            },
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }
    ngOnInit(): void {
        this.parentSubject.subscribe(event => {
            this.addFormGenerate();
            this.nameInputRef.show();
            if (event && event.docObj) {
                this.updateTheForm(event.docObj);
            }
        });
    }

    addFormGenerate() {
        this.addEditDocFb = this.fb.group({
            name: ['', Validators.required],
            contactNumber: ['', Validators.required],
            specialization: [''],
            totalExperience: [''],
            medicalRegistration: [''],
            membership: new FormArray([]),
            education: new FormArray([]),
            clinicServices: new FormArray([]),
            consultationFees: [''],
            establishment: [''],
            nameOfClinic: ['', Validators.required],
            clinicAddress: ['', Validators.required],
            numberOfSession: [''],
            medicalCarrierStart: [''],
            gender: ['', Validators.required],
            operationDays: new FormArray([])
        });
        this.addDay();
        this.addEditDocFb.valueChanges.pipe(
            debounceTime(800)
        ).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.addEditDocFb);
        });
    }

    addDay() {
        const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
        days.forEach(element => {
            let item = this.fb.group({
                day: [element],
                isActive: [true],
                timings: new FormArray([this.addTiming()])
            });
            this.operationDaysEle.push(item);
        });
    }

    addTiming() {
        return this.fb.group({
            startHour: [''],
            startMinute: [''],
            endHour: [''],
            endMinute: [''],
        });
    }

    updateTheForm(doctor) {
        this.addEditDocFb.addControl('_id', new FormControl());
        doctor.education.forEach(element => {
            this.addEducation();
        });
        doctor.membership.forEach(element => {
            this.addMembership();
        });
        doctor.clinicServices.forEach(element => {
            this.addClinicService();
        });
        // -- session
        doctor.operationDays.forEach((ele, index) => {
            if (ele && ele.timings && ele.timings.length > 1 ) {
                ele.timings.forEach((ses, i) => {
                    if (i > 0 ) {
                        const timeArr = this.getoperationDaysControls()[index].get('timings') as FormArray;
                        this.addNewSession(timeArr);
                    }
                });
            }
        });
        this.addEditDocFb.patchValue(doctor);
        this.nameInputRef.show();
    }

    submit(payload) {
        const docForm = { ...payload };
        const id = docForm._id;
        if (id === undefined) {
            this.docService.addDoctor(docForm).subscribe((res) => {
                this.close(res);
            });
        } else {
            delete docForm._id;
            this.docService.editDoctor(docForm, id).subscribe((res) => {
                this.close(res);
            });
        }
    }

    close(res) {
        if (res) {
            this.okClickBtn.emit(res.success);
        }
        this.nameInputRef.hide();
    }


    get membershipArray(): FormArray {
        return this.addEditDocFb.get('membership') as FormArray;
    }
    addMembership() {
        let fg = this.fb.control(null);
        this.membershipArray.push(fg);
    }
    removeMembership(idx: number) {
        this.membershipArray.removeAt(idx);
    }

    get educationArray(): FormArray {
        return this.addEditDocFb.get('education') as FormArray;
    }
    addEducation() {
        let fg = this.fb.control(null);
        this.educationArray.push(fg);
    }
    removeEducation(idx: number) {
        this.educationArray.removeAt(idx);
    }

    get clinicServicesArray(): FormArray {
        return this.addEditDocFb.get('clinicServices') as FormArray;
    }
    addClinicService() {
        let fg = this.fb.control(null);
        this.clinicServicesArray.push(fg);
    }
    removeClinicService(idx: number) {
        this.clinicServicesArray.removeAt(idx);
    }

    get operationDaysEle(): FormArray {
        return this.addEditDocFb.get('operationDays') as FormArray;
    }

    // ------------ UI------
    getmembershipControls() {
        return this.membershipArray.controls;
    }

    geteducationControls() {
        return this.educationArray.controls;
    }

    getclinicServicesControls() {
        return this.clinicServicesArray.controls;
    }

    getoperationDaysControls() {
        return this.operationDaysEle.controls;
    }

    addNewSession(timeAr: FormArray) {
        timeAr.push(this.addTiming());
    }

    removeSession(timeAr: FormArray, idx: number) {
       timeAr.removeAt(idx);
    }

    toggleIsWorking(fc: FormGroup, toggle: boolean) {
        if (!toggle) {
            fc.removeControl('timings');
        } else {
            fc.addControl('timings', new FormArray([this.addTiming()]));
        }
    }


}
