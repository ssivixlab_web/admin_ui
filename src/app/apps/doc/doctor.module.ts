import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import {SharedModule} from '../../theme/shared/shared.module';
import { DoctorService } from './doctor.service';
import { DocOverviewComponent } from './overview/overview.component';
import { DocBulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { DocSearchComponent } from './search/search.component';
import { DocAddEditComponent } from './search/addEdit/addEdit.component';
import { ArticleUploadComponent } from './article-upload/article-upload.component';

@NgModule({
  imports: [
    CommonModule,
    DoctorRoutingModule,
    SharedModule
  ],
  declarations: [
    DocOverviewComponent,
    DocBulkUploadComponent,
    DocSearchComponent,
    DocAddEditComponent,
    ArticleUploadComponent
  ],
  providers: [
    DoctorService
  ],
})
export class DoctorModule { }
