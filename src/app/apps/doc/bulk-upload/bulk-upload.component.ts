import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../doctor.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-doc-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.scss']
})
export class DocBulkUploadComponent implements OnInit {
  bulkUploadFb: FormGroup;
  inputFile: string = null;
  inputFileUpload: File;
  show: boolean = false;
  constructor(public docService: DoctorService, private fb: FormBuilder) {
    this.bulkUploadFb = this.fb.group({
      fileData: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
  }

  submit(pay) {
    this.docService.bulkUpload(this.inputFileUpload).subscribe((res) => {
      console.log('res', res);
      this.show = true;
    }, (err) => {
      console.log('err', err);
    });
  }

  openFile(fileupload: FileList) {
    if (fileupload.length > 0) {
      this.inputFileUpload = fileupload.item(0);
      this.inputFile = fileupload.item(0).name;
    }
  }

}
