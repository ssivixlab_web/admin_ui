import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../doctor.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-article-upload',
  templateUrl: './article-upload.component.html',
  styleUrls: ['./article-upload.component.scss']
})
export class ArticleUploadComponent implements OnInit {

  blogUploadFb: FormGroup;
  inputFile: string = null;
  inputFileUpload: File;
  show: boolean = false;
  blogListData: any = [];
  subscribtion: Subscription = new Subscription();
  constructor(public docService: DoctorService, private fb: FormBuilder, private blogList: DoctorService) {
    this.blogUploadFb = this.fb.group({
      fileData: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    this.getBlogAllList();
  }

  private getBlogAllList() {
    this.subscribtion.add(this.blogList.getBlogList().subscribe(res => {
      if (res.status && res.data) {
        this.blogListData = res.data;
      }
    }));
  }

  submit() {
    this.docService.blogUpload(this.inputFileUpload).subscribe((res) => {
      console.log('res', res);
      this.show = true;
    }, (err) => {
      console.log('err', err);
    });
  }

  openFile(fileupload: FileList) {
    if (fileupload.length > 0) {
      this.inputFileUpload = fileupload.item(0);
      this.inputFile = fileupload.item(0).name;
    }
  }
}
