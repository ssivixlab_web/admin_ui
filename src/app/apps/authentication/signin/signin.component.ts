import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppAuthenticationService } from 'src/app/theme/utility/authentication.service';
import { SESSION_STORAGE_KEY, SIGNIN_VALUE } from 'src/app/app-constants';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenericValidator } from 'src/app/theme/utility/generic.validators';
import { debounceTime } from 'rxjs/operators';
import { SigninService } from './signin.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, AfterViewInit {
  adminSignInFb: FormGroup;
  errorMsg: string;
  displayMessage: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;
  subscribtion: Subscription = new Subscription();
  constructor(private router: Router, private fb: FormBuilder, public signindata: SigninService) {
    this.adminSignInFb = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
    this.validationMessages = {
      email: {
        required: 'Email is required',
        email: 'Invalid Email'
      },
      password: {
        required: 'Password is required'
      }
    };
    this.genericValidator = new GenericValidator(this.validationMessages);

  }

  ngOnInit() {
    if (AppAuthenticationService.isAuthenticated()) {
      this.router.navigateByUrl('dashboard/analytics');
    }
  }

  ngAfterViewInit(): void {
    this.adminSignInFb.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.adminSignInFb);
    });
  }

  signin(payload) {
    this.subscribtion.add(this.signindata.signin(payload).subscribe(res => {
      if (res !== null && res.id !== null && res.roles[0] === "ROLE_ADMIN" && res.accessToken.length > 30 && res.active) {
        console.log('Signin data', res.accessToken.length)
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.TOKEN, res.accessToken);
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(res));
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_NAME, res.username);
        this.router.navigateByUrl('dashboard/analytics');
      } else {
        alert("Please enter a valid Username and Password")
      }

    }))
    // if (payload.email === SIGNIN_VALUE.EMAIL_ID && payload.password === SIGNIN_VALUE.PASSWORD) {
    //   this.errorMsg = '';
    //   AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.TOKEN, SIGNIN_VALUE.LOGEDIN_USERID);
    //   AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, SIGNIN_VALUE.USER);
    //   this.router.navigateByUrl('dashboard/analytics');
    // } else {
    //   this.errorMsg = 'User not Authoried';
    // }
  }

}
