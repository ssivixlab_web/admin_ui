import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigninComponent } from './signin.component';
import { SigninRoutingModule } from './signin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SigninService } from './signin.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SigninRoutingModule
  ],
  declarations: [SigninComponent],
  providers: [
    SigninService
  ]
})
export class SigninModule { }
