import { Component, OnInit, ViewChild } from '@angular/core';
import { ReferalService } from './referal.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UiModalComponent } from 'src/app/theme/shared/components/modal/ui-modal/ui-modal.component';
import { NgbDateStruct, NgbCalendar, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { IReferalResponse, IReferal } from './referal';
import { AnimationModalComponent } from 'src/app/theme/shared/components/modal/animation-modal/animation-modal.component';
import { CustomNgbDateAdapter } from 'src/app/theme/utility/custom-ngb-date-adapter.service';
import { DateUtility } from 'src/app/theme/utility/dateUtility.service';
import { DATE_FORMAT_UI } from 'src/app/app-constants';

@Component({
  selector: 'app-referal',
  templateUrl: './referal.component.html',
  styleUrls: ['./referal.component.scss']
})
export class ReferalComponent implements OnInit {
  @ViewChild('referalModal', {static: true}) referalModalRef: UiModalComponent;
  @ViewChild('msgModal', {static: true}) msgModalRef: AnimationModalComponent;
  msg: string = '';
  dateformatUI = DATE_FORMAT_UI;
  searchResultData: IReferal[] = [];
  addEditReferalFb: FormGroup;
  constructor(private refCodeService: ReferalService,
              private fb: FormBuilder,
              private dt: CustomNgbDateAdapter,
              private ngbCalendar: NgbCalendar,
              private dateAdapter: NgbDateAdapter<string>,
              private dateutil: DateUtility) {
    this.addEditReferalFb = this.fb.group({
      referralCode: new FormControl('', [Validators.required]),
      companyName: new FormControl('', [Validators.required]),
      couponValue: new FormControl('', [Validators.required]),
      countryCode : new FormControl('', [Validators.required]),
      valueType: new FormControl('', [Validators.required]),
      limit: new FormControl(),
      startDate: new FormControl('', [Validators.required]),
      EndDate: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
    this.getReferalList();
  }

  getReferalList() {
    this.refCodeService.getReferalCodeList().subscribe((res: IReferalResponse) => {
      this.searchResultData = res.data;
    });
  }

  showCardFooter() {
    return this.searchResultData.length > 0 ? false : true;
  }

  openModal() {
    this.addEditReferalFb.reset();
    if (this.addEditReferalFb.get('_id') != null) {
      this.addEditReferalFb.removeControl('_id');
    }
    this.referalModalRef.show();
  }

  refreshnow($ev) {
    this.getReferalList();
  }

  closeModal() {
    this.referalModalRef.hide();
  }

  edit(obj: IReferal) {
    this.openModal();
    this.addEditReferalFb.addControl('_id', new FormControl());
    this.addEditReferalFb.patchValue(obj);
  }

  delete(refId) {
    let delRef = {
      _id: refId,
      isActive: false
    };
    this.refCodeService.changeRefCode(delRef).subscribe((res) => {
      this.msg = 'Is InActive';
      this.msgModalRef.showSuccessModal();
    }, (err) => {
      console.log('err', err);
    });
  }

  submit(payload: IReferal) {
    if (payload.limit === '') {
      payload = {...payload};
      payload.limit = null;

    }
    payload = this.dateutil.updateStartandEndDate(payload);
    this.refCodeService.changeRefCode(payload).subscribe((res) => {
      this.msg = res.data;
      this.closeModal();
      this.msgModalRef.showSuccessModal();
    }, (err) => {
      console.log('err', err);
    });
  }

  getToday() {
    if (this.addEditReferalFb.get('_id') == null) {
      return this.dt.fromModel(this.dateAdapter.toModel(this.ngbCalendar.getToday()));
    }
  }

  getMaxDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditReferalFb.get('EndDate').value);
  }

  getMinDate(): NgbDateStruct {
    return this.dt.fromModel(this.addEditReferalFb.get('startDate').value);
  }
}
