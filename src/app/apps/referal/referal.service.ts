
import { AppHttpApiService } from 'src/app/theme/utility/http-api.service';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app-constants';
import { Observable, of } from 'rxjs';
import { IReferalResponse, IReferalOpRes } from './referal';

@Injectable()
export class ReferalService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getReferalCodeList(): Observable<IReferalResponse> {
        return this.httpApi.postData(`${API_URL.REFERAL_CODE_LIST}`, {});
    }

    changeRefCode(payload): Observable<IReferalOpRes> {
        return this.httpApi.postData(`${API_URL.REFERAL_CODE_CHANGE}`, payload);
    }

}
