export interface IReferalResponse {
    data: IReferal[];
    status: boolean;
}

export interface IReferalOpRes {
    data: string;
    status: boolean;
}

export interface IReferal {
    EndDate: string;
    companyName: string;
    isActive: boolean;
    limit: string|number;
    referralCode: string;
    referralCreatedOn: string;
    referralUsed: string|number;
    startDate: string;
    valueType:string;
    _id: string;
}
