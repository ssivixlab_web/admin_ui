import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferalRoutingModule } from './referal-routing.module';
import { ReferalComponent } from './referal.component';
import {SharedModule} from '../../theme/shared/shared.module';
import { ReferalService } from './referal.service';

@NgModule({
  declarations: [ReferalComponent],
  imports: [
    CommonModule,
    ReferalRoutingModule,
    SharedModule
  ],
  providers: [
    ReferalService
  ]
})
export class ReferalModule { }
