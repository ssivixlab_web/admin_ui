import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReferalComponent} from './referal.component';

const routes: Routes = [
  {
    path: '',
    component: ReferalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferalRoutingModule { }
