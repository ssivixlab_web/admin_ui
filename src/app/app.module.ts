import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './theme/shared/shared.module';

import { AppComponent } from './app.component';

import { ToggleFullScreenDirective } from './theme/shared/full-screen/toggle-full-screen';

import { AppHttpApiService } from './theme/utility/http-api.service';
import { AppAuthGuardService } from './theme/utility/auth-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from './theme/layout/layout.module';

@NgModule({
  declarations: [
    AppComponent,
    ToggleFullScreenDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule
  ],
  providers: [AppHttpApiService, AppAuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
